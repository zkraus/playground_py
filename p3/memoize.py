from __future__ import print_function
import functools


def counter(function):
    function.calls = 0

    @functools.wraps(function)
    def _counter(*args, **kwargs):
        function.calls += 1
        return function(*args, **kwargs)

    return _counter

@functools.lru_cache(maxsize=3)
@counter
def fibonacci(n):
    if n < 2:
        return n
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)


if __name__ == '__main__':
    print(fibonacci(100))
    print(fibonacci.__wrapped__.__wrapped__.calls)
