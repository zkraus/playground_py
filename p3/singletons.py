import functools
from collections import namedtuple


# Singleton by wrapper ------------------------

def singleton_wrapper(cls):
    """Singleton design patter using wrapper

    :param cls: wrapped class
    :type cls: type

    :return: singleton wrapped class
    :rtype: type
    """
    instances = dict()

    @functools.wraps(cls)
    def _singleton(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]

    return _singleton


@singleton_wrapper
class WrappedSpam(object):

    def __init__(self):
        print('{} init'.format(self.__class__.__name__))


class WrappedInheritedSpam(WrappedSpam.__wrapped__):
    pass


# Singleton by class --------------------------------

class SingletonClass(object):
    _instance = None

    def __init__(self):
        print('{} init'.format(self.__class__.__name__))

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls, *args, **kwargs)

        return cls._instance


class ClassInheritedSpam(SingletonClass):
    pass


# Singleton by metaclass --------------------------------

class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        print('{} meta'.format(cls.__name__))
        if cls not in cls._instances:
            cls._instances[cls] = super.__call__(*args, **kwargs)

        return cls._instances[cls]


class MetaSpam(object, metaclass=SingletonMeta):

    def __init__(self):
        print('{} init'.format(self.__class__.__name__))


class MetaInheritedSpam(MetaSpam):
    pass


if __name__ == '__main__':

    TypePair = namedtuple('TypePair', ['base', 'inherited'])

    type_pair_list = [
        TypePair(WrappedSpam, WrappedInheritedSpam),
        TypePair(SingletonClass, ClassInheritedSpam),
        TypePair(MetaSpam, MetaInheritedSpam),
    ]

    for base, inherited in type_pair_list:
        print('{} -> {}'.format(base.__name__, inherited.__name__))

        a = base()
        b = base()
        c = inherited()
        d = inherited()

        print(id(a))
        print(id(b))
        print(id(c))
        print(id(d))

        print()

        print(type(a))
        print(type(b))
        print(type(c))
        print(type(d))

        print('-' * 16)
