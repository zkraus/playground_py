from multiprocessing import Pool
import random

import time
from multiprocessing.pool import ThreadPool
from threading import Thread

import asyncio


def get_data(size: int = 256, dataset: str = 'abcdefghijklmnopqrstuvwxyz0123456789'):
    tmp = [random.choice(dataset) for i in range(size)]
    return tmp


def do_task(in_data: list):
    i = 10000
    tmp = None
    ts_start = time.time()
    while i:
        tmp = sorted(in_data)
        i -= 1
    ts_stop = time.time()
    t_elapsed = ts_stop - ts_start
    return t_elapsed
    # return tmp


def sequential(in_dataset):
    for i, data in enumerate(in_dataset):
        result = do_task(data)
        print("{}: {}".format(i, result))


def threaded(in_dataset):
    threads = []

    for i, data in enumerate(in_dataset):
        thread = Thread(target=do_task, args=[data], name="thr_%d" % i)
        threads.append(thread)
        thread.start()

    while threads:
        thread = threads.pop()  # type: Thread
        thread.join()

        print("{}: {}".format(thread.getName(), 'xxx'))


def thread_pool(in_dataset, thread_count=None):
    if not thread_count:
        thread_count = len(in_dataset)
    with ThreadPool(processes=thread_count) as pool:
        result = pool.map(do_task, in_dataset)
    for i, result in enumerate(result):
        print("{}: {}".format(i, result))


def process_pool(in_dataset, process_count=None):
    if not process_count:
        process_count = len(in_dataset)
    with Pool(process_count) as pool:
        result = pool.map(do_task, in_dataset)
    for i, result in enumerate(result):
        print("{}: {}".format(i, result))


async def do_task_async(in_data: list):
    i = 10000
    tmp = None
    ts_start = time.time()
    while i:
        tmp = sorted(in_data)
        i -= 1
    ts_stop = time.time()
    t_elapsed = ts_stop - ts_start
    print(t_elapsed)
    return t_elapsed
    # return tmp


async def do_task_async_wrapper(in_dataset):
    await asyncio.wait([do_task_async(x) for x in in_dataset])


def async_io(in_dataset):
    loop = asyncio.get_event_loop()

    loop.run_until_complete(
        do_task_async_wrapper(in_dataset)
    )

    loop.close()


def timing(procedure, in_dataset):
    print(procedure.__name__)
    ts_start = time.time()
    procedure(*in_dataset)
    ts_end = time.time()
    t_elapsed = ts_end - ts_start
    print(t_elapsed)
    return t_elapsed


# data = get_data()

data_list = [get_data() for x in range(32)]

if __name__ == '__main__':
    timing(sequential, [data_list])
    timing(threaded, [data_list])
    timing(thread_pool, [data_list, 4])
    timing(process_pool, [data_list, 4])
    timing(async_io, [data_list])
