def one():
    yield 1


def two():
    yield 1
    yield 2


def myrange(start_stop, step_stop=None, stop=None):
    step = 1
    if step_stop is None and stop is None:
        begin = 0
        end = start_stop
    elif stop is None:
        begin = start_stop
        end = step_stop
    else:
        begin = start_stop
        step = step_stop
        end = stop
    if step > 0:
        while begin < end:
            yield begin
            begin += step
    else:
        while begin > end:
            yield begin
            begin += step


print(list(myrange(10)))
print(list(myrange(1, 10)))
print(list(myrange(1, 3, 10)))
print(list(myrange(100, -5, 1)))


def input_generator():
    i = 1
    while True:
        val = (yield i)
        if val is not None:
            i = val
        else:
            i += 1


gen = input_generator()

print(next(gen))
print(next(gen))
print(gen.send(10))
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
