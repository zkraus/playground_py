import copy


class Node:
    """..warning:: DO NOT IN ANY CASE MAKE A CYCLE IN THE LIST"""

    def __init__(self, data):
        """Initialized to a Node implementation of single linked list

        :param data: user data
        :type data: any
        """
        self._data = data
        self._next = None

    def __str__(self):
        try:
            return "%s -> %s" % (self._data, self._next)
        except RecursionError:
            return "LOOP!"

    def __copy__(self):
        tmp = Node(self._data)
        tmp._next = self._next
        return tmp

    # noinspection PyDefaultArgument
    def __deepcopy__(self, memodict={}):
        tmp = Node(self._data)
        dst = tmp
        src = self
        while src._next:
            if id(src._next) in memodict:
                break
            memodict[id(src._next)] = src._next
            # noinspection PyProtectedMember
            dst._next = Node(src._next._data)
            dst = dst._next
            src = src._next
        return tmp

    def __contains__(self, item):
        ptr = self
        while ptr._data != item:
            if ptr._next is None:
                break
            ptr = ptr._next
        return ptr._data == item

    def __iter__(self):
        ptr = self
        while ptr:
            yield ptr.data
            ptr = ptr._next
        raise StopIteration

    def __getitem__(self, item):
        if not isinstance(item, int):
            raise AttributeError("only ints for indexes")
        ptr = self
        while item:
            if not ptr._next:
                raise IndexError
            ptr = ptr._next
            item -= 1
        return ptr

    def __len__(self):
        i = 0
        ptr = self
        while True:
            # this has to be done like that, while ptr: will cause recursion error on __deepcopy__
            if ptr is None:
                break
            ptr = ptr._next
            i += 1
        return i

    def append(self, data):
        """Append data to end of the list

        :param data: user data to put in list
        :type data: any
        """
        ptr = self
        while ptr._next is not None:
            ptr = ptr._next
        ptr._next = Node(data)

    def insert(self, data):
        """Insert data here and now after current item

        :param data: user data to put in list
        :type data: any
        """
        ptr = self._next
        self._next = Node(data)
        self._next._next = ptr

    def join(self, node):
        """Join a linked list represented by Node

        :param node: linked list to join
        :type node: Node
        """

        ptr = self
        while ptr._next is not None:
            ptr = self._next
        ptr._next = node

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value

    @staticmethod
    def from_list(list_data):
        """Helper to make a single linked list using Node implementation from
        python list object

        :param list_data: user data to create list from
        :type list_data: list[any]

        :return: created linked list using Node
        :rtype: Node or None
        """

        result = None

        for item in reversed(list_data):
            item = Node(item)
            if result is not None:
                item.join(result)
            result = item

        return result


list_a = Node.from_list(['a', 'b', 'c'])

list_c = copy.copy(list_a)
list_d = copy.deepcopy(list_a)

print("list_a: %s" % list_a)
print("list_c: %s" % list_c)
print("list_d: %s" % list_d)

print("----")
print("changing node in list_c (which is shallow copy of list_a")
list_c[1].data = 'x'

print("list_a: %s" % list_a)
print("list_c: %s" % list_c)
print("list_d: %s" % list_d)

print("---")

print('Contains: %s' % ('c' in list_a))

for i in list_a:
    print(i)

print(list_a[1])

print(len(list_a))
