class A:
    """Baseclass with slots"""
    __slots__ = ['x']


def X():
    class B:
        """Baseclass with slots based in local namespace of a function"""
        __slots__ = ['x']

    return B()


def Y():
    """Baseclass with slots spawned from local namespace of a function"""
    return A()


def Z(slots):
    class C(A):
        """Child class with slots, from class with slots, based in local namespace of a function"""
        __slots__ = slots
        pass

    return C()


def ZD(slots):
    class C(D):
        """Child class with slots, from class with empty slots, based in local namespace of a function"""
        __slots__ = slots
        pass

    return C()


def decor1(cls):
    """Decorator to inherit class and force slots"""
    class X(cls):
        __slots__ = ['x']

    return X

def decor2(cls):
    """Decorator to modify a class and force slots"""
    cls.__slots__ = ['x']
    return cls

class D:
    """Baseclass with empty slots"""
    __slots__ = []
    pass


@decor1
class E:
    """Baseclass without slots"""
    pass

class F(D):
    """Child class with slots from class with empty slots"""
    __slots__ = ['x']

@decor2
class G:
    """Baseclass without slots"""
    pass

@decor1
class H:
    """Baseclass with empty slots"""
    __slots__ = []

@decor2
class I:
    """Baseclass with empty slots"""
    __slots__ = []

def test(obj):
    name = obj.__class__.__name__

    print(f'class {name}')
    print(type(obj))

    obj.x = 1
    try:
        obj.y = 2
    except AttributeError:
        print(f'{name}.y -> Attribute error check')

    print()


if __name__ == '__main__':
    items = [A(), X(), Y(), Z(['x']), E(), ZD(['x']), F(), G(), H(), I()]

    for item in items:
        test(item)
