# Zkraus' Python Playground

## Contents:

Within these playground, there are various examples, and tryouts of code, techniques, etc.
For example: pointers, lists, annotations, iterators,...

## Authors:
* Zdenek Kraus <zdenek.kraus@gmail.com>


## default licenses
for others non specified (if applicable):

* **LGPL 3.0** for code items
* **CC-BY-SA-3.0** for non-code art items (if applicable)

