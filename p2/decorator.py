#!/usr/bin/env python

global_opts = {}

var = "test"


def opt(variable, val=None):
    def wrap(func):
        print func.__name__
        setattr(func, variable, val)
        global_opts[variable] = val
        return func

    return wrap


def opt_empty(func):
    print func.__name__
    return func


def opt_one(one):
    print "opt_one decoration"
    def decorator(func):
        print "opt_one decorator"
        def func_wrap():
            print "opt_one decoration funct wrap"
            return func(one)
        return func_wrap
    return decorator


def my_test_func():
    print my_test_func.__dict__


@opt_empty
def my_test_func2():
    print my_test_func2.__dict__


@opt("test", "val")
def my_test_func3():
    print my_test_func3.__dict__


@opt("test", "val")
@opt("est", "al")
def my_test_func4():
    print my_test_func4.__dict__


@opt_empty
def no_call():
    print "No call"


@opt_one(var)
def par(one):
    print "par: %s" % one

print "--"

par()

print "--"
my_test_func()
my_test_func2()
print my_test_func3.__dict__
my_test_func3()
my_test_func4()

print '--'
print "global_opts: %s" % global_opts
