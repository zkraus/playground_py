

class Base(object):
    def __init__(self):
        super(Base, self).__init__()
        print self.__class__.__name__
        print "Base init()"


class BaseA(Base):
    def __init__(self):
        super(BaseA, self).__init__()
        print self.__class__.__name__
        print "BaseA init()"
        self.a = 'string BaseA'

    def xa(self):
        print "%s x() %s" % (self.__class__.__name__, self.a)

    def x(self):
        print "%s x() %s" % (self.__class__.__name__, self.a)


class BaseB(Base):
    def __init__(self):
        super(BaseB, self).__init__()
        print self.__class__.__name__
        print "BaseB init()"
        self.b = 'string BaseB'

    def xb(self):
        print "%s x() %s" % (self.__class__.__name__, self.b)

    def x(self):
        print "%s x() %s" % (self.__class__.__name__, self.b)


class C(BaseA, BaseB):
    def __init__(self):
        super(C, self).__init__()
        print self.__class__.__name__
        print "C init()"



#class D(BaseB, BaseA):  # this raise MRO TypeError exception
class D(BaseA, BaseB):
    def __init__(self):
        super(D, self).__init__()
        print self.__class__.__name__
        print "D init()"

class E(C):
    def __init__(self):
        super(E, self).__init__()
        print self.__class__.__name__
        print "E init()"


print C.__mro__
print D.__mro__
print E.__mro__
c = C()
d = D()
e = E()
