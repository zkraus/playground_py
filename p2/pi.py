
import fractions
import random
import math

MAX_COUNT = 1000
MAX_RAND = 120


def pi_approx(coprimes, counts):
  x = float(coprimes)/float(counts)
  result = math.sqrt(6/x)
  #print "  pi_approx: %d/%d -> %f" % (coprimes, counts, result)
  return result


def get_coprimes(max_count, max_rand):
  coprime = count = 0
  while count < max_count:
    a = random.randint(1, max_rand)
    b = random.randint(1, max_rand)
    gcd = fractions.gcd(a, b)
    if gcd == 1:
      coprime += 1
    count += 1
  #print "get_coprimes(%d, %d) -> %d" % (max_count, max_rand, coprime)
  return coprime

def rolling_pi_approx(max_count, max_rand):
  coprime = count = result = 0

  print "\n-- max_count: %d, max_rand: %d" % (max_count, max_rand)
  while count < max_count:
    a = random.randint(1, max_rand)
    b = random.randint(1, max_rand)
    gcd = fractions.gcd(a, b)
    if gcd == 1:
      coprime += 1
    count += 1

    if coprime > 0:
      pia = pi_approx(coprime, count)
      diff = pia - math.pi
      diff_p = diff/math.pi * 100
      if count % 100 == 0:
        print "\r[%d / %d] -> %f (%f, %f%%)" % (coprime, count, pia, diff, diff_p),

  return pia



rolling_pi_approx(100000000, 10000000)


if False:
  max_count_factor = 10000
  max_rand_factor = 100
  for i in xrange(1,9):
    for k in xrange(1,9):
      mcount = i * max_count_factor
      mrand = k * max_rand_factor
      print "\n-- max_count: %d, max_rand: %d --" % (mcount, mrand)
      for l in xrange(1,10):
        coprimes = get_coprimes(mcount, mrand)
        pia = pi_approx(coprimes, mcount)
        diff = pia - math.pi
        diff_p = diff/math.pi * 100
        print "  [%d]:: cnt: %d, rnd: %d, cop: %d -> %f (%f, %f%%)" % (l, mcount, mrand, coprimes, pia, diff, diff_p)






