
class Table(object):
    hhline = "="
    hline = "-"
    vline = "|"
    corner = "+"

    def __init__(self, headers):
        self.headers = headers
        self.data = []

        self._in_sep = " %s " % (self.vline,)
        self._l_sep = "%s " % (self.vline,)
        self._r_sep = " %s" % (self.vline,)
        self._in_line = "%s%s%s" % (self.hline, self.corner, self.hline,)
        self._l_line = "%s%s" % (self.corner, self.hline,)
        self._r_line = "%s%s" % (self.hline, self.corner,)
        self._in_hline = "%s%s%s" % (self.hhline, self.corner, self.hhline,)
        self._l_hline = "%s%s" % (self.corner, self.hhline,)
        self._r_hline = "%s%s" % (self.hhline, self.corner,)

    def _columns_width(self):
        tmp = [ 0 for _ in self.headers ]
        for row in [self.headers] + self.data:
            col_i = 0
            for col in row:
                col_len = len(col)
                if col_len > tmp[col_i]:
                    tmp[col_i] = col_len
                col_i += 1

        return tmp

    def _draw_xline(self, h, l, i, r):
        tmp = "%s%s%s" % (l, i.join([h*i for i in self._columns_width()]), r)
        return tmp

    def _draw_line(self):
        return self._draw_xline(self.hline, self._l_line, self._in_line, self._r_line)

    def _draw_hline(self):
        return self._draw_xline(self.hhline, self._l_hline, self._in_hline, self._r_hline)

    def _draw_row(self, row):
        aligned_row = []
        columns_width = self._columns_width()
        item_i = 0
        for item in row:
            item_len = len(item)
            item_align = " " * (columns_width[item_i] - item_len)
            item_i += 1
            aligned_row.append("%s%s" %(item, item_align))
        tmp = "%s%s%s" % (self._l_sep, self._in_sep.join(aligned_row), self._r_sep)
        return tmp

    def _draw_table(self):
        tmp = []
        tmp.append(self._draw_line())
        tmp.append(self._draw_row(self.headers))
        tmp.append(self._draw_hline())
        for row in self.data:
            tmp.append(self._draw_row(row))
        tmp.append(self._draw_line())
        return "\n".join(tmp)

    def __str__(self):
        return self._draw_table()


tab = Table([ 'header', 'properties', 'a', 'b', 'item' ])
tab.data = [
            ['asdf', 'asdf', 'adsf', 'b', 'asdf'],
            ['asdfxxxx', 'assgfrdf', 'addsf', 'c', 'asdf'],
]

print tab


import tabulate
print tabulate.tabulate(tab.data, tab.headers, tablefmt='fancy_grid')