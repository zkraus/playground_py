import copy

# input data
n = range(10)
m = range(10, 20)

# List comprehension
i = copy.copy(n)

# filter out even numbers
# correctly working with outer scopes
i = [i for i in i if i % 2 == 0]
#               ^ this one is outer scope
print "list comprehension: %s" % i

# it does rewrites outer variable
ii = [i for i in i if i % 2 == 0]
#               ^ this one is outer scope
print "list comprehension: %s" % ii
print "new var i: %s" % i
del i
print

# generator comprehension
# filter numbers in 'n' greater than 4
t = (i for i in n if i > 4)
print "Tuple comprehension: %s" % t
print

# Dict comprehension
d = {o: p for o, p in zip(n, m) if p % 3 == 0}
print "Dict comprehension: %s" % d

#  list comprehensions do not enclose the scope
l = [z for z in range(10) if z > 5]
print "basic list comprehension (using var 'z'): %s" % l
print "let's check var 'z': %s" % z

# dict comprehensions do enclose the scope
d = {o: p for o, p in zip(n, m) if o % 3 == 0}
print "Dict comprehension: %s" % d
try:
    print "var o:"
    print o
except NameError as exc:
    print exc

try:
    print "var p:"
    print p
except NameError as exc:
    print exc

print


# multilevel comprehension (basic)
a = range(0, 10)
b = range(10, 100, 10)
ml = [x + y for x in a for y in b if x * 10 == y]
print "Multilevel comprehension (basic): %s" % ml
ml2 = [x + y for y in b for x in a if x * 10 == y]
print "Multilevel comprehension (basic, reversed layers): %s" % ml2

# multilevel comprehension (advanced)
# iterate over list generated in the comprehension from outer list
a = [
    [1, 2, 3, 4, 5, 6],
    [1, 3, 5],
    [2, 4, 6],
]

ml3 = [x for y in a for x in y if x % 2 == 0 and x % 3 == 0]
print "Multilevel comprehension (advanced): %s" % ml3

# clean the environment
del x
del y

try:
    ml4 = [x for x in y for y in a if x % 2 == 0 and x % 3 == 0]
    print "Multilevel comprehension (advanced, reversed layers): %s" % ml4
except NameError as exc:
    print "Multilevel comprehension (advanced, reversed layers): %s" % exc



# eof
