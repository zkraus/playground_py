import unittest
import doctests

class TestStatisticalFunctions(unittest.TestCase):

    def test_average(self):
        self.assertEqual(doctests.average([20, 30, 70]), 40.0)
        self.assertEqual(round(doctests.average([1, 5, 7]), 1), 4.3)
        with self.assertRaises(ZeroDivisionError):
            doctests.average([])
        with self.assertRaises(TypeError):
            doctests.average(20, 30, 70)

unittest.main() # Calling from the command line invokes all tests