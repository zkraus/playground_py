import copy
import math

class Progress(object):
    VALUE = lambda x: "%d/%d" % (x.value, x.max_value)
    PERCENT = lambda x: "%6.2f%%" % (x.progress*100)

    def __init__(self, max_value = 100, value = 0, format = VALUE):
        self.value = value
        self.max_value = max_value
        self.format = format

    def __str__(self):
        return self.format(self)

    @property
    def progress(self):
        return float(self.value)/float(self.max_value)

    def __type_check(self, other):
        if not isinstance(other, int):
            raise TypeError("Cannot add other value than int")

    def __iadd__(self, other):
        self.__type_check(other)
        if self.value + other > self.max_value:
            self.value = self.max_value
        else:
            self.value += other
        return self

    def __add__(self,other):
        tmp = copy.copy(self)
        tmp += other
        return tmp

    __radd__ = __add__

    def __isub__(self, other):
        self.__type_check(other)
        if self.value - other < 0:
            self.value = 0
        else:
            self.value -= other
        return self

    def __sub__(self, other):
        tmp = copy.copy(self)
        tmp -= other
        return tmp

    def __rsub__(self, other):
        tmp = copy.copy(self)
        if other - tmp.value < 0:
            tmp.value = 0
        else:
            tmp.value = other - tmp.value
        return tmp

    def __int__(self):
        return self.value


class ProgressBar(Progress):
    BAR_PLAIN = { 'l': '[', 's': '=', 'n': ' ', 'z': ' ', 'c':'>', 'm': '=', 'r': ']'}
    BAR_FLAT = { 'l': '|', 's': '=', 'n': ' ', 'z': ' ', 'c':'-', 'm': '=', 'r': '|'}
    BAR_STAR = { 'l': '', 's': '*', 'n': '.', 'z': '.', 'c':'*', 'm': '*', 'r': ''}
    BAR_LINE = { 'l': '', 's': '_', 'n': '_', 'z': '|', 'c':'|', 'm': '|', 'r': ''}
    BAR_CURSOR = { 'l': '[', 's': ' ', 'n': ' ', 'z': '|', 'c':'>', 'm': '*', 'r': ']'}

    def __init__(self, max_value = 100, value = 0, format = Progress.VALUE, length = 32, bar_format = BAR_PLAIN):
        super(ProgressBar, self).__init__(max_value, value, format)
        self.length = length
        self.bar_format = bar_format
        self.__get_act_length()

    def __get_act_length(self):
        act_length = self.length - len(self.bar_format['l'] + self.bar_format['r'])
        if act_length < 2:
            raise Exception("Insufficient length given")
        return act_length

    def __bar(self):
        tmp = ''
        act_length = self.__get_act_length()
        steps = int(math.ceil(act_length * self.progress))
        tmp += self.bar_format['l']
        if (steps > 1):
            tmp += self.bar_format['s'] * (steps - 1)
        if (steps > 0):
            if (self.value == self.max_value):
                tmp += self.bar_format['m']
            else:
                tmp += self.bar_format['c']
        step_offset = 0
        if (steps == 0):
            tmp += self.bar_format['z']
            step_offset = 1
        tmp += self.bar_format['n'] * (act_length - steps - step_offset)
        tmp += self.bar_format['r']
        return tmp

    def __str__(self):
        tmp = self.__bar()
        tmp += ' %s' % (self.format(self))
        return tmp

if __name__ == '__main__':
    for x_bar_format in [ProgressBar.BAR_PLAIN, ProgressBar.BAR_FLAT, ProgressBar.BAR_LINE, ProgressBar.BAR_STAR, ProgressBar.BAR_CURSOR]:
        prg2 = ProgressBar(format = ProgressBar.PERCENT, bar_format=x_bar_format)
        print prg2
        prg2 += 1
        print prg2
        prg2 -= 1
        for i in xrange(9):
            prg2 += 10
            print prg2
        prg2 += 9
        print prg2
        prg2 += 1
        print prg2





