#!/usr/bin/env python
import sys
import socket
import pickle

import optparse


def _error(in_msg):
    print >> sys.stderr, "%s:Error:%s" % (__file__, in_msg)
    sys.exit(1)


arg_parser = optparse.OptionParser(description="Log snapper control program")
arg_parser.add_option('-p', '--port', help='Log sniffer agent port', type=int, dest='port')
arg_parser.add_option('-a', '--address', help='Address to connect to', dest='address')
arg_parser.add_option('-d', '--data', help='Data for selected action, if required',
                      dest='data', action='append')

(options, args) = arg_parser.parse_args()

if not options.port:
    _error("port is required parameter")
if not options.address:
    _error("address is required parameter")

sock = socket.socket(socket.AF_INET)
sock.connect((options.address, options.port))
sock.settimeout(30)

send_data = "\n".join(options.data)
sock.send(send_data)
print "Data sent: %s" % (options.data,)
sock.close()
